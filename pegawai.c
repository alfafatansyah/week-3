#include <stdio.h>
#include <string.h>

typedef struct pegawai
{
    char nama[20];
    int umur;
    char id[20];
    char posisi[20];
} mypegawai_t;

int main()
{
    mypegawai_t pegawai1;

    strcpy(pegawai1.nama, "punk01");
    pegawai1.umur = 25;
    strcpy(pegawai1.id, "14045");
    strcpy(pegawai1.posisi, "SET");

    printf("Nama: %s, umur %d, id: %s, posisi: %s", pegawai1.nama, pegawai1.umur, pegawai1.id, pegawai1.posisi);
}