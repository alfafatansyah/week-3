#include <stdio.h>
#include <string.h>

typedef struct mykendaraan
{
    char tipe[20];
    char warna[20];
    int unit;
} mobil_t;

int main()
{
    mobil_t car[3];
    char input[20];
    char input2[20];
    int a = 0;
    int b = 0;
    int c = 0;
    int tanda = 0;

    strcpy(car[0].tipe, "honda");
    strcpy(car[0].warna, "merah");
    car[0].unit = 10;

    strcpy(car[1].tipe, "toyota");
    strcpy(car[1].warna, "oranye");
    car[1].unit = 20;

    strcpy(car[2].tipe, "daihatsu");
    strcpy(car[2].warna, "kuning");
    car[2].unit = 30;

    while (1)
    {
        printf("\r\nInput nama mobil :");
        scanf("%s", input);

        for (int i = 0; i < 3; i++)
        {
            a = strcmp(input, car[i].tipe);

            if (a == 0)
            {
                printf("\rMobil tipe %s tersedia, warna %s, jumlah unit %d", car[i].tipe, car[i].warna, car[i].unit);

                printf("\r\nInput warna mobil yang akan diambil :");
                scanf("%s", input2);
                b = strcmp(input2, car[i].warna);

                if (b == 0)
                {
                    printf("\rWarna mobil tersedia :");
                    printf("\r\nInput jumlah unit yang akan diambil :");
                    scanf("%d", &c);

                    car[i].unit -= c;
                    printf("\rBerhasil mengambil mobil %s sebanyak %d unit :", car[i].tipe, c);
                    printf("\r\nSisa unit mobil %s adalah %d unit\n", car[i].tipe, car[i].unit);
                    break;
                }

                else
                {
                    printf("Warna mobil tidak tersedia.\n");
                    break;
                }
            }
        }
        if (a != 0)
            printf("\nNama mobil tidak tersedia.\n");
    }
    return 0;
}
