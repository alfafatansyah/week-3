#include <stdio.h>

typedef struct tambahkurang
{
    int a1, a2, tambah, kurang;
} angka;

int penjumlahan(angka x);
int pengurangan(angka x);

int main()
{
    angka aritmatika;

    aritmatika.a1 = 5;
    aritmatika.a2 = 6;

    aritmatika.tambah = penjumlahan(aritmatika);
    printf("\nhasil penjumlahan %d + %d = %d", aritmatika.a1, aritmatika.a2, aritmatika.tambah);

    aritmatika.kurang = pengurangan(aritmatika);
    printf("\nhasil penjumlahan %d - %d = %d", aritmatika.a1, aritmatika.a2, aritmatika.kurang);

    return 0;
}

int penjumlahan(angka x)
{
    x.tambah = x.a1 + x.a2;
    return x.tambah;
}
int pengurangan(angka x)
{
    x.kurang = x.a1 - x.a2;
    return x.kurang;
}