#include <stdio.h>
#include <string.h>

typedef struct checksum
{
    char header;
    char datasize;
    char address;
    char value1;
    char value2;
    char checksum;
} data;

void printdata (data h, data d, data a, data v1, data v2, data c);

int main()
{
    data input, cek;

    cek.header = 30;

    while (1)
    {
        printf("\nInput header value (30): ");
        scanf("%d", &input.header);

        if (input.header == cek.header)
        {
            printf("\nInput data size (1 - 2): ");
            scanf("%d", &input.datasize);

            if (input.datasize < 3 && input.datasize > 0)
            {
                printf("\nInput address (1 - 5): ");
                scanf("%d", &input.address);

                if (input.address < 6 && input.address > 0)
                {
                    printf("\nInput value 1 (hihgbyte): ");
                    scanf("%d", &input.value1);

                    if (input.datasize > 1)
                    {
                        printf("\nInput value 2 (lowbyte): ");
                        scanf("%d", &input.value2);
                    }

                    cek.checksum = input.header - input.datasize - input.address - input.value1 - input.value2;

                    printf("\nInput checksum (%d) : ", cek.checksum);
                    scanf("%d", &input.checksum);

                    if (input.checksum == cek.checksum)
                    {
                        printf("\nInput success, data output = %d\n", cek.checksum);
                        break;
                    }
                    else
                        printf("\n❌ Invalid checksum.");
                }
                else
                    printf("\n❌ Invalid address.");
            }
            else
                printf("\n❌ Invalid data size.");
        }
        else
            printf("\n❌ Invalid header value.");

        printf("\n");
    }

    return 0;
}

void printdata (data h, data d, data a, data v1, data v2, data c){
    
}