#include <stdio.h>
#include <string.h>

typedef struct checksum
{
    char name[9];
    unsigned char data;
} data;

int checkvalue(data a[]);
void printoutput(data b[], unsigned int c);

int main()
{
    data input[6];
    unsigned char cek[5], cekcs;
    unsigned int finish;

    strcpy(input[0].name, "header");
    strcpy(input[1].name, "datasize");
    strcpy(input[2].name, "address");
    strcpy(input[3].name, "highbyte");
    strcpy(input[4].name, "lowbyte");
    strcpy(input[5].name, "checksum");

    cek[0] = 30;  // header
    cek[1] = 2;   // datasize
    cek[2] = 5;   // address
    cek[3] = 255; // highbyte value
    cek[4] = 255; // lowbyte value

    while (1)
    {

        printf("\nInput %s data (30) : ", input[0].name);
        scanf("%d", &input[0].data);

        if (input[0].data == cek[0])
        {

            for (int i = 1; i < 6; i++)
            {
                printf("\nInput %s data (1 - %d): ", input[i].name, cek[i]);
                scanf("%d", &input[i].data);
                if (i == 5)
                {
                    if (cekcs == input[5].data)
                    {

                        printoutput(input, finish);
                        // break;
                        return 0;
                    }

                    else
                    {
                        printf("\n❌ Invalid value.");
                        break;
                    }
                }

                if (input[i].data <= cek[i])
                {
                    if (input[1].data == 1 && i == 3)
                    {
                        input[4].data = 0;

                        i++;
                    }

                    if (i == 4)
                    {
                        cekcs = checkvalue(input);
                        cek[5] = cekcs; // checksum value
                    }

                    continue;
                }

                else
                {
                    printf("\n❌ Invalid value.");
                    break;
                }
            }
        }

        else
            printf("\n❌ Invalid value.");

        printf("\n");
    }
}

int checkvalue(data a[])
{
    return (0 - a[1].data - a[2].data - a[3].data - a[4].data);
}

void printoutput(data b[], unsigned int c)
{
    printf("\nOutput : ");

    for (int i = 1; i < 6; i++)
    {
        if (i == 3)
        {
            c = b[i].data;
            c <<= 8;
            c = c + b[i + 1].data;

            printf("%d ", c);
            i++;
            continue;
        }

        printf("%d ", b[i].data);
    }

    printf("\nDone.");
}