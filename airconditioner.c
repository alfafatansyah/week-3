#include <stdio.h>

unsigned char input[9];
unsigned char limit[9];
unsigned short data[3];

void countdata();
void printout();

int main()
{
    limit[0] = 4;   // limit header
    limit[1] = 6;   // limit data load
    limit[2] = 3;   // limit register
    limit[3] = 255; // limit value
    limit[4] = 255; // limit value
    limit[5] = 255; // limit value
    limit[6] = 255; // limit value
    limit[7] = 255; // limit value
    limit[8] = 255; // limit value
    limit[9];       // true checksum value

    while (1)
    {
        printf("\n<< Remote AC >>");
        printf("\ndata[1]   = header \ndata[2]   = data load \ndata[3]   = register \ndata[4]++ = value \ndata[10]  = checksum\n\n");

        printf("Masukkan data[1] (%d) : ", limit[0]);
        scanf("%d", &input[0]);

        if (input[0] == limit[0])
        {
            printf("✅ Valid value %d.\n", input[0]);

            for (int i = 1; i <= 9; i++)
            {

                printf("Masukkan data[%d] (%d) : ", i + 1, limit[i]); // Input data
                scanf("%d", &input[i]);

                if (input[2] > 1 && input[1] > 1)
                {
                    printf("❌ Invalid register.\n");
                    break;
                }

                if (input[i] <= limit[i] && input[i] >= 0) // check input in range limit
                    printf("✅ Valid value %d.\n", input[i]);

                else // If input out of limit
                {
                    printf("❌ Invalid value.\n");
                    break;
                }

                if (input[2] == 2)
                    limit[3] = 160;

                if (input[2] == 3)
                    limit[3] = 1;

                if (input[1] == i - 2)
                {
                    limit[9] = 0 - input[1] - input[2] - input[3] - input[4] - input[5] - input[6] - input[7] - input[8];
                    i = 8;
                }

                if (i == 9)
                {
                    countdata();
                    printout();
                    return 0;
                }
            }
        }

        else
        {
            printf("❌ Invalid value.\n");
        }
    }

    return 0;
}

void countdata()
{
    for (int i = 0; i < 3; i++)
    {
        data[i] = input[i + i + 3];
        data[i] <<= 8;
        data[i] += input[i + i + 3 + 1];
    }
}

void printout()
{
    switch (input[2])
    {
    case 1:
        for (int i = 0; i < 3; i++)
        {
            printf("\nSensor suhu %d : %d", i + 1, data[i]);
        }

        break;
    case 2:
        printf("\nKecepatan putar kipas : %d", input[3]);
        break;
    case 3:
        if (input[3] == 1)
            printf("\nstatus AC : ON");
        else
            printf("\nstatus AC : OFF");
        break;
    }
}