#include <stdio.h>

int main()
{
    int a = 17;
    int *p = &a;

    printf("\n%d", a);    // output value of a
    printf("\n%p", &a);   // output memory address of a
    printf("\n%p", p);    // output memory address of pointer p -->> a
    printf("\n%d\n", *p); // output value of pointer p -->> a

    printf("\n----------\n");
    // pointer in array

    int b[5] = {11, 22, 33, 44, 55};
    int *pp = b;

    printf("\n%p", pp);      // output memory address of pointer pp -->> array
    printf("\n%d", *pp);     // output value of pointer pp -->> array
    printf("\n%d", *++pp);   // output value of pointer "next" pp -->> array
    printf("\n%d", ++*pp);   // output value of pointer increase by 1 in pp -->> array
    printf("\n%d", *--pp);   // output value of pointer "previously" pp -->> array
    printf("\n%d\n", --*pp); // output value of pointer decrease by 1 in pp -->> array

    printf("\n----------\n\n");
    // pointer in array part 2

    char c[3][5] = {{'A', 'L', 'F', 'A', 'F'},
                    {'A', 'T', 'A', 'N', 'S'},
                    {'Y', 'A', 'H', '1', '7'}};

    //char **ppp = &c;
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                printf("%c ", (*(*(c + i) + j)));
            }
            
        }
        

    return 0;
}