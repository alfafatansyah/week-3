#include <stdio.h>

typedef struct databyte
{
    int value;
} mybyte;

void separate(mybyte a, mybyte b, mybyte c);

int main()
{
    mybyte input, highbyte, lowbyte;

    //input.value = 666;
    printf("\nInput value : ");
    scanf("%d", &input.value);

    separate(input, highbyte, lowbyte);

    return 0;
}

void separate(mybyte a, mybyte b, mybyte c)
{
    b.value = 0;
    c.value = 0;

    for (int i = 1 << 7; i > 0; i = i >> 1)
    {
        b.value = (a.value & i << 8) ? b.value | i : b.value | 0;
        c.value = (a.value & i) ? c.value | i : c.value | 0;
    }

    printf("\nhighbyte value = %d", b.value);
    printf("\nlowbyte value = %d", c.value);
}