#include <stdio.h>

void separate(int a, int b, unsigned char c);

int main()
{
    int input, highbyte;
    unsigned char lowbyte;

    while (1)
    {
        printf("\nInput value : ");
        scanf("%d", &input);

        if (input > 65635)
            printf("Input melebihi 16 bit");
        else
            break;
    }

    separate(input, highbyte, lowbyte);

    return 0;
}

void separate(int a, int b, unsigned char c)
{
    b = a >> 8;
    c = a;

    printf("\nhighbyte value = %d", b);
    printf("\nlowbyte value = %d", c);
}
